#include "stdafx.h"
#include "PlayerInfo.h"
#include <fstream>
#include <iostream>
#include <istream>

using namespace std;

PlayerInfo::PlayerInfo(int id1, string name1, string dateOfBirth1, string country1, string email1, //Constructor
	int level1Score1, int currentLevel1, int remainingLives1, int bulletCount1, int maxLevelReach1,
	string language1, int volumeLevel1, int graphicsQuality1, int difficultyLevel1)
{
	id = id1;
	name = name1;
	dateOfBirth = dateOfBirth1;
	country = country1;
	email = email1;
	level1Score = level1Score1;
	currentLevel = currentLevel1;
	remainingLives = remainingLives1;
	bulletCount = bulletCount1;
	maxLevelReach = maxLevelReach1;
	language = language1;
	volumeLevel = volumeLevel1;
	graphicsQuality = graphicsQuality1;
	difficultyLevel = difficultyLevel1;
}

void PlayerInfo::saveLocalInfo() { //Save player info to file
	ofstream myfile;
	myfile.open(to_string(id) + "_info.txt"); //Create unique ID
	myfile << id << endl;
	myfile << name << endl;
	myfile << dateOfBirth << endl;
	myfile << country << endl;
	myfile << email << endl;
	myfile << level1Score << endl;
	myfile << currentLevel << endl;
	myfile << remainingLives << endl;
	myfile << bulletCount << endl;
	myfile << maxLevelReach << endl;
	myfile << language << endl;
	myfile << volumeLevel << endl;
	myfile << graphicsQuality << endl;
	myfile << difficultyLevel << endl;
	myfile.close();
	cout << "Saved player " << id << " locally"  << endl;
}

void PlayerInfo::localOverwriteInfo() { //Overwrite info from local save (May be required if player can edit profile externally)
	string line;
	ifstream myfile(to_string(id) + "_info.txt");
	if (myfile.is_open()) {
		int i = 0;
		while (myfile, getline(myfile,line))
		{
			if (i == 1) { name = line; }
			if (i == 2) { dateOfBirth = line; }
			if (i == 3) { country = line; }
			if (i == 4) { email = line; }
			if (i == 5) { level1Score = atoi(line.c_str()); }
			if (i == 6) { currentLevel = atoi(line.c_str()); }
			if (i == 7) { remainingLives = atoi(line.c_str()); }
			if (i == 8) { bulletCount = atoi(line.c_str()); }
			if (i == 9) { maxLevelReach = atoi(line.c_str()); }
			if (i == 10) { language = line; }
			if (i == 11) { volumeLevel = atoi(line.c_str()); }
			if (i == 12) { graphicsQuality = atoi(line.c_str()); }
			if (i == 13) { difficultyLevel = atoi(line.c_str()); }
			i++;
		}
		cout << "Player " << id << " overwritten by text file" << endl;
	}
	else {
		cout << "Unable to open player file" << endl;
	}
	myfile.close();
}




