#pragma once
#include <string>

using namespace std;

class PlayerInfo
{
public:
	PlayerInfo(int, string, string, string, string, 
		int, int, int, int, int, 
		string, int, int, int);

	//Public so info can be accessed by SaveJson
	int id;  //Player Info
	string name;
	string dateOfBirth;
	string country;
	string email;
	int level1Score; //Player game data
	int currentLevel;
	int remainingLives;
	int bulletCount;
	int maxLevelReach;
	string language; //Player preferences
	int volumeLevel;
	int graphicsQuality;
	int difficultyLevel;

	void saveLocalInfo();
	void localOverwriteInfo();
};

