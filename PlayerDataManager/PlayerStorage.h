#pragma once
#include "stdafx.h"
#include <vector>
#include "PlayerInfo.h"


class PlayerStorage //Storage to easily track players
{
public:
	vector <PlayerInfo> players; //Player pool 
	int addPlayer(PlayerInfo);
	void removePlayer(PlayerInfo);
};
