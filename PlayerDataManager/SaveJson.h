#pragma once
#include "stdafx.h"
#include "PlayerInfo.h"
#include "PlayerStorage.h"

class SaveJson{
public:
	void save(PlayerInfo);
	void load(PlayerInfo);
	int loadNewPlayerFile(PlayerStorage, int); //Assumes ids are unique globally and playerfiles exist in file system
};