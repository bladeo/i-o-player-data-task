// PlayerDataManager.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <iostream>
#include "PlayerInfo.h"
#include "SaveJson.h"

int main()
{
	SaveJson jsonStream;
	PlayerStorage playerStore;
	PlayerInfo player1(1, "test", "11/12/1990", "England", "Test@Gmail.com", 1020, 1, 22, 653, 12,"English", 6, 2, 1);
	int player1Loc = playerStore.addPlayer(player1); //Example create player and save in storage

	//Interaction can now be controlled through player pool by returned storage index reference
	playerStore.players[player1Loc].saveLocalInfo(); //Not a perfect solution, methods should be used for this process instead of direct vector access
	playerStore.players[player1Loc].localOverwriteInfo();

	jsonStream.save(playerStore.players[player1Loc]); //Save to Json
	jsonStream.load(playerStore.players[player1Loc]); //Load from Json
	int player2Loc = jsonStream.loadNewPlayerFile(playerStore, 2); //Load new player from file (possibly from other system or client)

	system("pause");
    return 0;
}

