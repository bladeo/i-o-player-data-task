#include "stdafx.h"
#include <iostream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "SaveJson.h"
#include "PlayerInfo.h"
#include "PlayerStorage.h"

using namespace std;
namespace pt = boost::property_tree;

void SaveJson::save(PlayerInfo player) { //Save for external use
	pt::ptree j;
	j.put("id", player.id);
	j.put("name", player.name);
	j.put("dateOfBirth", player.dateOfBirth);
	j.put("country", player.country);
	j.put("email", player.email);
	j.put("level1Score", player.level1Score);
	j.put("currentLevel", player.currentLevel);
	j.put("remainingLives", player.remainingLives);
	j.put("bulletCount", player.bulletCount);
	j.put("maxLevelReach", player.maxLevelReach);
	j.put("language", player.language);
	j.put("volumeLevel", player.volumeLevel);
	j.put("graphicsQuality", player.graphicsQuality);
	j.put("difficultyLevel", player.difficultyLevel);
	pt::write_json("Player " + to_string(player.id) + "_info.json", j);

	cout << "Saved player " << player.id << " to json" << endl;
}

void SaveJson::load(PlayerInfo player) { //Load file (Will overwrite current player data)
	pt::ptree root;
	pt::read_json("Player " + to_string(player.id) + "_info.json", root);

	player.name = root.get<string>("name");
	player.id = root.get<int>("id");
	player.dateOfBirth = root.get<string>("dateOfBirth");
	player.country = root.get<string>("country");
	player.email = root.get<string>("email");
	player.level1Score = root.get<int>("level1Score");
	player.currentLevel = root.get<int>("currentLevel");
	player.remainingLives = root.get<int>("remainingLives");
	player.bulletCount = root.get<int>("bulletCount");
	player.maxLevelReach = root.get<int>("maxLevelReach");
	player.language = root.get<string>("language");
	player.volumeLevel = root.get<int>("volumeLevel");
	player.graphicsQuality = root.get<int>("graphicsQuality");
	player.difficultyLevel = root.get<int>("difficultyLevel");

	cout << "Player " << player.id << " info updated" << endl;
}

int SaveJson::loadNewPlayerFile(PlayerStorage storage, int id) { //Loads a player file from the file system
	pt::ptree root;
	pt::read_json("Player " + to_string(id) + "_info.json", root); //Load using id, assumes ids are unique globally

	string name = root.get<string>("name");
	int id1 = root.get<int>("id");
	string dateOfBirth = root.get<string>("dateOfBirth");
	string country = root.get<string>("country");
	string email = root.get<string>("email");
	int level1Score = root.get<int>("level1Score");
	int currentLevel = root.get<int>("currentLevel");
	int remainingLives = root.get<int>("remainingLives");
	int bulletCount = root.get<int>("bulletCount");
	int maxLevelReach = root.get<int>("maxLevelReach");
	string language = root.get<string>("language");
	int volumeLevel = root.get<int>("volumeLevel");
	int graphicsQuality = root.get<int>("graphicsQuality");
	int difficultyLevel = root.get<int>("difficultyLevel");

	PlayerInfo newPlayer(id1, name, dateOfBirth, country, email, 
		level1Score, currentLevel, remainingLives, bulletCount, maxLevelReach,
		language, volumeLevel, graphicsQuality, difficultyLevel);
	
	storage.addPlayer(newPlayer);
	cout << "Player " << id1 << " loaded and stored" << endl;
	return size(storage.players) - 1;
}